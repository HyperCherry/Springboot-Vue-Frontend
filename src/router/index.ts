import { createRouter, createWebHistory } from 'vue-router'
import loginView from '../views/Login.vue'
import mainView from "../views/MainView.vue"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: loginView
    },
    {
      path: '/main',
      name: 'main',
      component: mainView
      // component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
