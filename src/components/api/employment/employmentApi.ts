import { type AxiosPromise } from "axios"
import { service, type RestResponse } from "@/components/api/base"
import { type EmploymentModel } from '@/components/api/model/employmentModel'

export function getAllEmployments(size: number, current: number): AxiosPromise<RestResponse<any>> {
    return service({
        url: `/employment/getLists?size=${size}&current=${current}`,
        method: 'get',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    });
}

export function deleteEmployment(eid: string): AxiosPromise<RestResponse<any>> {
    var formData = new FormData();
    formData.append("eid", eid!!);
    return service({
        url: '/employment/delete',
        method: 'post',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        data: formData
    });
}

export function updateEmployment(employment: EmploymentModel): AxiosPromise<RestResponse<any>> {
    var formData = new FormData();
    formData.append('eid', String(employment.eid!!))
    formData.append('ename', employment.ename!!)
    formData.append('idcard', employment.idcard!!)
    formData.append('eaddr', employment.eaddr!!)
    formData.append('enumber', employment.enumber!!)
    formData.append('eidentity', String(employment.eidentity)!!)
    return service({
        url: '/employment/update',
        method: 'post',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        data: formData
    })
}

export function insertEmployment(employment: EmploymentModel):AxiosPromise<RestResponse<any>> {
    var formData = new FormData();
    formData.append('eid', String(employment.eid!!))
    formData.append('ename', employment.ename!!)
    formData.append('idcard', employment.idcard!!)
    formData.append('eaddr', employment.eaddr!!)
    formData.append('enumber', employment.enumber!!)
    formData.append('eidentity', String(employment.eidentity)!!)
    return service({
        url: '/employment/insert',
        method: 'post',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        data: formData
    })
}