import { type AxiosPromise } from "axios"
import { service, type RestResponse } from "@/components/api/base"
import { type UserModel } from "../model/userModel";

export function loginApi(data: UserModel): AxiosPromise<RestResponse<any>> {
    var formData = new FormData();
    formData.append("username", data.username!!);
    formData.append("loginname", data.loginname!!);
    formData.append("password", data.password!!);
    return service({
        url: '/user/getAdminByName',
        method: 'post',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        data: formData
    });
}

export function updatePassword(uid: number, password: string): AxiosPromise<RestResponse<any>> {
    var formData = new FormData();
    formData.append("uid", String(uid));
    formData.append("password", password);
    return service({
        url: '/user/updatePassword',
        method: 'post',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        data: formData
    });
}

export function getAllUserName(): AxiosPromise<RestResponse<any>> {
    return service({
        url: '/user/getAllUserName',
        method: 'post',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    });
}

export function changeAdmin(oldId:number,newId:number): AxiosPromise<RestResponse<any>> {
    var formData = new FormData();
    formData.append("oldId", String(oldId));
    formData.append("newId", String(newId));
    return service({
        url: '/user/updateAdmin',
        method: 'post',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        data: formData
    });
}