import { type AxiosPromise } from "axios"
import { service, type RestResponse } from "@/components/api/base"
import { type BuildingModel } from '@/components/api/model/buildingModel'

export function getAllBuildings(size: number, current: number): AxiosPromise<RestResponse<any>> {
    return service({
        url: `/building/getLists?size=${size}&current=${current}`,
        method: 'get',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    });
}

export function deleteBuilding(bid: string): AxiosPromise<RestResponse<any>> {
    var formData = new FormData();
    formData.append("eid", bid!!);
    return service({
        url: '/building/delete',
        method: 'post',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        data: formData
    });
}

export function updateBuilding(building:BuildingModel):AxiosPromise<RestResponse<any>> {
    var formData = new FormData();
    formData.append('bid', building.bid!!)
    formData.append('bname', building.bname!!)
    formData.append('bunit', building.bunit!!)
    formData.append('bfloor', String(building.bfloor!!))
    formData.append('bnumber', String(building.bnumber)!!)
    formData.append('barea', String(building.barea)!!)
    return service({
        url: '/building/update',
        method: 'post',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        data: formData
    })
}

export function insertBuilding(building:BuildingModel):AxiosPromise<RestResponse<any>> {
    var formData = new FormData();
    formData.append('bid', building.bid!!)
    formData.append('bname', building.bname!!)
    formData.append('bunit', building.bunit!!)
    formData.append('bfloor', String(building.bfloor!!))
    formData.append('bnumber', String(building.bnumber)!!)
    formData.append('barea', String(building.barea)!!)
    return service({
        url: '/building/insert',
        method: 'post',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        data: formData
    })
}