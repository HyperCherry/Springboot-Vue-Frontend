export interface BuildingModel {
    bid?: string,
    bname?: string,
    bunit?: string,
    bfloor?: number,
    bnumber?: number,
    barea?: number
}