export interface EmploymentModel {
    eid?: string;
    ename?: string;
    idcard?:string;
    eaddr?:string;
    enumber?:string;
    eidentity?:boolean;
}