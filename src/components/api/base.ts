import axios from "axios"
export const service = axios.create({
    baseURL: "http://localhost:8988",
    timeout: 50000,
    headers: { 'Content-Type': 'application/json;charset=utf-8' }
});

export interface RestResponse<T> {
    code?: number;
    msg?: string;
    data?: T;
}